#FROM openjdk:8-jdk-alpine
#FROM openjdk:8-jre
#FROM openjdk:17-oracle
FROM openjdk:11-jdk-alpine
ADD target/SinavApi-0.0.1-SNAPSHOT app.jar
ENTRYPOINT ["java","-jar","app.jar"]
